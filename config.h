/*  ____ _____  */
/* |  _ \_   _|  Derek Taylor (DistroTube) */
/* | | | || |  	http://www.youtube.com/c/DistroTube */
/* | |_| || |  	http://www.gitlab.com/dwt1/ */
/* |____/ |_|  	*/ 

/* See LICENSE file for copyright and license details. */
/* appearance */
static const unsigned int borderpx = 2;   /* border pixel of windows */
static const unsigned int snap     = 32;  /* snap pixel */
static const unsigned int gappx    = 6;   /* pixel gap between clients */
static const int showbar           = 1;   /* 0 means no bar */
static const int topbar            = 1;   /* 0 means bottom bar */
static const int horizpadbar       = 6;   /* horizontal padding for statusbar */
static const int vertpadbar        = 7;   /* vertical padding for statusbar */
/* Mononoki Nerd Font must be installed from AUR nerd-fonts-complete.
 * Otherwise, your default font will be Hack which is found in the standard
 * Arch repos and is listed as a dependency for this build. JoyPixels is also
 * a hard dependency and makes colored fonts and emojis possible.
 */
static const char *fonts[]     = {"Mononoki Nerd Font:size=9:antialias=true:autohint=true",
                                  "Hack:size=8:antialias=true:autohint=true",
                                  "JoyPixels:size=10:antialias=true:autohint=true"
						     	};
static const char col_1[]  = "#282c34"; /* background color of bar */
static const char col_2[]  = "#282c34"; /* border color unfocused windows */
static const char col_3[]  = "#d7d7d7";
static const char col_4[]  = "#924441"; /* border color focused windows and tags */
/* bar opacity 
 * 0xff is no transparency.
 * 0xee adds wee bit of transparency.
 * Play with the value to get desired transparency.
 */
static const unsigned int baralpha    = 0xee; 
static const unsigned int borderalpha = OPAQUE;
static const char *colors[][3]        = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_3, col_1, col_2 },
	[SchemeSel]  = { col_3, col_4,  col_4 },
};
static const unsigned int alphas[][3] = {
	/*               fg      bg        border     */
	[SchemeNorm] = { OPAQUE, baralpha, borderalpha },
	[SchemeSel]  = { OPAQUE, baralpha, borderalpha },
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };
// static const char *tags[] = { "", "", "", "", "", "", "", "", "" };


static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class              instance    title                                            tags mask     isfloating   monitor */
	{ "NULL",             NULL,       "Android Emulator - Pixel_3a_API_30_x86:5554",   1 << 3,       1,           -1 },
	{ "NULL",             NULL,       "Android Emulator - Pixel_3a_API_30_x86:5556",   1 << 3,       1,           -1 },
	{ "NULL",             NULL,       "Android Emulator - Nexus_4_API_30:5554",        1 << 3,       1,           -1 },
	{ "NULL",             NULL,       "Android Emulator - Nexus_4_API_30:5556",        1 << 3,       1,           -1 },
	{ "NULL",             NULL,       "Emulator",                                      1 << 3,       1,           -1 },
	{ "NULL",             NULL,       "Emulator",                                      1 << 3,       1,           -1 },
	{ "jetbrains-studio", NULL,       "Emulator",                                      1 << 1,       0,           -1 },
	{ "jetbrains-idea",   NULL,       "Emulator",                                      1 << 2,       0,           -1 },
	{ "st-256color",      NULL,       "java",                                          1 << 6,       0,           -1 },
	{ "st-256color",      NULL,       "npm",                                           1 << 6,       0,           -1 },
	{ "st-256color",      NULL,       "mvn",                                           1 << 6,       0,           -1 },
	{ "blueman-manager",  NULL,       "Bluetooth Devices",                             0,            1,           -1 },
	{ "Onboard",          NULL,       "Onboard",                                       0,            1,           -1 },
};

/* layout(s) */
static const float mfact     = 0.50; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */

#include "layouts.c"
static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
	{ "HHH",      grid },
	{ NULL,       NULL },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },
#define CMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* dmenu */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
/* If you are using the standard dmenu program, use the following. */
static const char *dmenucmd[]    = { "j4-dmenu-desktop" };
/* If you are using the dmenu-distrotube-git program, use the following for a cooler dmenu! */
/* static const char *dmenucmd[]    = { "dmenu_run", "-g", "10", "-l", "48", "-p", "Run: ", NULL }; */

/* the st terminal with tabbed */
/*static const char *termcmd[]     = { "st", NULL };*/
static const char *termcmd[]     = { "alacritty", NULL };
/* An alternative way to launch st along with the fish shell */
/* static const char *termcmd[]     = { "st", "-e fish", NULL }; */
static const char *tabtermcmd[]  = { "tabbed", "-r", "2", "st", "-w", "''", NULL };

static Key keys[] = {
	/* modifier             key        function        argument */
	{ MODKEY,               XK_w,      spawn,          {.v = dmenucmd } },
	{ MODKEY|ShiftMask,     XK_Return, spawn,          {.v = termcmd } },
	{ Mod4Mask,             XK_Return, spawn,          {.v = tabtermcmd } },
	{ MODKEY,               XK_b,      togglebar,      {0} },
	{ MODKEY|ShiftMask,     XK_j,      rotatestack,    {.i = +1 } },
	{ MODKEY|ShiftMask,     XK_k,      rotatestack,    {.i = -1 } },
	{ MODKEY,               XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,               XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,               XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,               XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,               XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,               XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY|ControlMask,   XK_Return, zoom,           {0} },
	{ MODKEY,               XK_Tab,    view,           {0} },
	{ MODKEY,               XK_c,      killclient,     {0} },

    /* Layout manipulation */
	{ MODKEY,               XK_Tab,    cyclelayout,    {.i = -1 } },
	{ MODKEY|ShiftMask,     XK_Tab,    cyclelayout,    {.i = +1 } },
	{ MODKEY,               XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,     XK_space,  togglefloating, {0} },

    /* Switch to specific layouts */
	{ MODKEY,               XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,               XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,               XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,               XK_g,      setlayout,      {.v = &layouts[3]} },

	{ MODKEY,               XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,     XK_0,      tag,            {.ui = ~0 } },

	// AUDIO
	//                      XFAudioRaiseVolume
	{ 0,                    0x1008ff13,  spawn,          CMD("pamixer -i 5; pkill -RTMIN+10 dwmblocks") },
	//                      XFAudioLowerVolume
	{ 0,                    0x1008ff11,  spawn,          CMD("pamixer -d 5; pkill -RTMIN+10 dwmblocks") },
	//                      XFAudioMute
	{ 0,                    0x1008ff12,  spawn,          CMD("pamixer -t") },
	//                      XFAudioMicMute
	{ 0,                    0x1008ffb2,  spawn,          CMD("pamixer --default-source -t") },
	// 			XFAudioPlay
	{ 0,                    0x1008ff14,  spawn,          CMD("playerctl play-pause") },
	// 			XFAudioPrev
	{ 0,                    0x1008ff16,  spawn,          CMD("playerctl previous") },
	// 			XFAudioNext
	{ 0,                    0x1008ff17,  spawn,          CMD("playerctl next") },


	//SUSPEND
	{ MODKEY,               XK_F12,      spawn,          CMD("systemctl suspend") },
	

	// BRIGHTNESS
	{ 0,                    0x1008ff02,  spawn,          CMD("xbacklight -inc 5; pkill -RTMIN+7 dwmblocks") },
	{ 0,                    0x1008ff03,  spawn,          CMD("xbacklight -dec 5; pkill -RTMIN+7 dwmblocks") },

	// Flameshot
	{ 0,                    XK_Print,    spawn,          CMD("flameshot gui") },

	// Onboard
	//                      XF86TaskPane
	{ 0,                    0x1008ff7f,  spawn,          CMD("thinkpad-rotate flip") },


    /* Switching between monitors */
	{ MODKEY,               XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,               XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,     XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,     XK_period, tagmon,         {.i = +1 } },
	
    /* Apps Launched with SUPER + ALT + KEY */
	//{ MODKEY|Mod4Mask,        XK_b,    spawn,          CMD("tabbed -r 2 surf -pe x '.surf/html/homepage.html'") },
	//{ MODKEY|Mod4Mask,        XK_c,    spawn,          CMD("st -e cmus") },
	//{ MODKEY|Mod4Mask,        XK_e,    spawn,          CMD("st -e emacsclient -c -a emacs") },
	//{ MODKEY|Mod4Mask,        XK_f,    spawn,          CMD("st -e vifm") },
	{ MODKEY|Mod1Mask,        XK_h,    spawn,          CMD("alacritty -e htop") },
	{ MODKEY|Mod1Mask,        XK_b,    spawn,          CMD("blueman-manager") },
	{ MODKEY|Mod1Mask,        XK_f,    spawn,          CMD("firefox") },
	{ MODKEY|Mod1Mask,        XK_p,    spawn,          CMD("pavucontrol") },
	//{ MODKEY|Mod4Mask,        XK_i,    spawn,          CMD("st -e irssi") },
	//{ MODKEY|Mod4Mask,        XK_l,    spawn,          CMD("st -e lynx gopher://distro.tube") },
	{ MODKEY,                 XK_n,    spawn,          CMD("alacritty -e nnn") },
	//{ MODKEY|Mod4Mask,        XK_r,    spawn,          CMD("st -e rtv") },
	
    /* Dmenu scripts launched with ALT + CTRL + KEY */
	{ Mod1Mask|ControlMask, XK_e,      spawn,          CMD("./.dmenu/dmenu-edit-configs.sh") },
	{ Mod1Mask|ControlMask, XK_m,      spawn,          CMD("./.dmenu/dmenu-sysmon.sh") },
	{ Mod1Mask|ControlMask, XK_p,      spawn,          CMD("passmenu") },
	{ Mod1Mask|ControlMask, XK_r,      spawn,          CMD("./.dmenu/dmenu-reddio.sh") },
	{ Mod1Mask|ControlMask, XK_s,      spawn,          CMD("./.dmenu/dmenu-surfraw.sh") },
	{ Mod1Mask|ControlMask, XK_i,      spawn,          CMD("./.dmenu/dmenu-scrot.sh") },
    
	TAGKEYS(                  XK_1,          0)
	TAGKEYS(                  XK_2,          1)
	TAGKEYS(                  XK_3,          2)
	TAGKEYS(                  XK_4,          3)
	TAGKEYS(                  XK_5,          4)
	TAGKEYS(                  XK_6,          5)
	TAGKEYS(                  XK_7,          6)
	TAGKEYS(                  XK_8,          7)
	TAGKEYS(                  XK_9,          8)
	{ MODKEY|ShiftMask,       XK_q,	   quit,		   {0} },
    { MODKEY|ShiftMask,       XK_r,    quit,           {1} }, 
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click           event mask   button          function        argument */
	{ ClkLtSymbol,     0,           Button1,        setlayout,      {0} },
	{ ClkLtSymbol,     0,           Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,     0,           Button2,        zoom,           {0} },
	{ ClkStatusText,   0,           Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,    MODKEY,      Button1,        movemouse,      {0} },
	{ ClkClientWin,    MODKEY,      Button2,        togglefloating, {0} },
	{ ClkClientWin,    MODKEY,      Button3,        resizemouse,    {0} },
	{ ClkTagBar,       0,           Button1,        view,           {0} },
	{ ClkTagBar,       0,           Button3,        toggleview,     {0} },
	{ ClkTagBar,       MODKEY,      Button1,        tag,            {0} },
	{ ClkTagBar,       MODKEY,      Button3,        toggletag,      {0} },
};


