#! /bin/bash 
setxkbmap hu
# setxkbmap -option caps:swapescape
# setxkbmap -option altwin:swap_lalt_lwin
xset r rate 500 100
picom &
nitrogen --restore &
dwmblocks &
redshift -l 46.2677:20.1403 & 
udiskie &
